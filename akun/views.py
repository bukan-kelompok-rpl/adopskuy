from django.shortcuts import render, HttpResponseRedirect, redirect
from django.contrib.auth import login, logout, authenticate 
from django.contrib import messages
from .models import User

def register_view(request):
    if(request.method == "POST"):
        nama_lengkap = request.POST['namaLengkap']
        alamat = request.POST['alamat']
        no_telp = request.POST['noTelp']
        email = request.POST['emailVar']
        password = request.POST['passwordVar']

        validation, check_msg = check_validity(email, password)
        if(validation == True):
            buat_akun(nama_lengkap, alamat, no_telp, email, password)
            check_msg = 'Akun berhasil dibuat! Selamat datang.'
            messages.success(request, check_msg)
            user = authenticate(request, username=email, password=password)
            login(request, user);   
            request.session.set_expiry(7200)
            return HttpResponseRedirect('/')
        else:
            messages.error(request, check_msg)
            return HttpResponseRedirect('/akun/daftar')
    else:
        context = {
            'page_title': 'Daftar | Adopskuy!'
        }

        return render(request, 'register.html', context)

def login_view(request):
    if(request.method == 'POST'):
        username = request.POST['emailForm']
        password = request.POST['passwordForm']
        user = authenticate(request, username=username, password=password)

        if(user is not None ):  
            login(request, user);   
            request.session.set_expiry(7200)
            return HttpResponseRedirect('/')
        else:
            messages.error(request, "Email/password tidak cocok! Silakan coba lagi.")
    context = {
        'page_title': 'Masuk | Adopskuy!'
    }

    return render(request, 'login.html', context)

def buat_akun(nama_lengkap, alamat, no_telp, email, password):
    User.objects.create_user(email, nama_lengkap, alamat, no_telp, password)
    return True

def check_validity(email, password):
    email_pswd_check = check_validity_helper(email, password)
    if(email_pswd_check == True):
        email_not_exist = check_email_not_exist(email)
        message = "Valid!" if email_not_exist else "Email sudah terdaftar sebelumnya! Silakan gunakan email lain."
        return email_not_exist, message
    message = "Email/password tidak valid!"
    return False, message;

def check_email_not_exist(email):
     return not User.objects.filter(email=email).exists()

def check_validity_helper(email, password):
    try:
        split_email = email.split("@")
        split_domain = split_email[1].split(".")
    except:
        return False

    len_check = len(password) >= 8
    upper_check = any(c.isupper() for c in password)
    lower_check = any(c.islower() for c in password)
    num_check = any(c.isnumeric() for c in password)

    return len_check and upper_check and lower_check and num_check

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/akun/masuk')
