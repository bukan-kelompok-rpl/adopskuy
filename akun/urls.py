from django.urls import path

from .views import register_view, login_view, logout_view

app_name = 'akun'

urlpatterns = [
    path('daftar/', register_view, name='daftar'),
    path('masuk/', login_view, name='login'),
    path('keluar/', logout_view, name='keluar')
]
