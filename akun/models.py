from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UserManager(BaseUserManager):
    def create_user(self, email, nama_lengkap=None, alamat=None, phone_number=None, password=None, is_active=True, is_staff=False, is_admin=False):
        if not email:
            raise ValueError("Users must have an email address")
        if not password:
            raise ValueError("Users must have a password")
        user_obj = self.model(
            email = self.normalize_email(email),
            nama_lengkap=nama_lengkap,
            alamat=alamat,
            phone_number=phone_number,
        )
        user_obj.set_password(password) # change user password
        user_obj.staff = is_staff
        user_obj.admin = is_admin
        user_obj.active = is_active
        user_obj.save(using=self._db)
        return user_obj

    def create_staffuser(self, email,nama_lengkap=None, alamat = None, phone_number = None, password=None):
        user = self.create_user(
                email,
                nama_lengkap=nama_lengkap,
                alamat=alamat,
                phone_number=phone_number,
                password=password,
                is_staff=True
        )
        return user

    def create_superuser(self, email, nama_lengkap=None, alamat = None, phone_number = None , password=None):
        user = self.create_user(
                email,
                nama_lengkap=nama_lengkap,
                alamat=alamat,
                phone_number=phone_number,
                password=password,
                is_staff=True,
                is_admin=True
        )
        return user


class User(AbstractBaseUser):
    email               = models.EmailField(max_length=55, unique=True)
    nama_lengkap           = models.CharField(max_length=55)
    alamat                 = models.CharField(max_length=3000)
    phone_number        = models.CharField(max_length=15)
    active              = models.BooleanField(default=True) # can login
    staff               = models.BooleanField(default=False) # staff user non superuser
    admin               = models.BooleanField(default=False) # superuser
    timestamp           = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['nama_lengkap', 'alamat', 'phone_number'] #['nama_lengkap'] #python manage.py createsuperuser

    objects = UserManager()

    def __str__(self):
        return self.nama_lengkap

    def get_nama_lengkap(self):
        if self.nama_lengkap:
            return self.nama_lengkap
        return self.email

    def get_short_name(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_active(self):
        return self.active
