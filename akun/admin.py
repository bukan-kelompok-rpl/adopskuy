from django.contrib import admin
from django.contrib.auth import get_user_model
from .models import User, UserManager
from .forms import UserAdminChangeForm, UserAdminCreationForm
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.safestring import mark_safe

User = get_user_model()

# Register your models here.
class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('nama_lengkap','email', 'admin', 'phone_number', 'alamat')
    list_filter = ('email','admin', 'staff', 'active')
    fieldsets = (
        (None, {'fields': ('nama_lengkap', 'email', 'password','phone_number','alamat')}),
        ('Permissions', {'fields': ('admin', 'staff', 'active',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('nama_lengkap','email', 'password1', 'password2', 'phone_number','alamat')}
        ),('Permissions', {'fields': ('admin', 'staff', 'active',)}),
    )
    search_fields = ('email', 'nama_lengkap',)
    ordering = ('email',)
    filter_horizontal = ()

admin.site.register(User, UserAdmin)
