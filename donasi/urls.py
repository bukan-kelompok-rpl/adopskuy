from django.urls import path

from . import views

app_name = 'donasi'


urlpatterns = [
    path('', views.home, name='home'),
    path('bayar/', views.bayar, name='bayar'),
    path('d/<int:donasi_id>/', views.detail, name='detail'),
    path('d/<int:donasi_id>/kirim', views.kirim, name='kirim'),
    path('gambar/', views.ubah_gambar, name='ubah-gambar'),
    path('admin/', views.admin_home, name='admin-home'),
    path('admin/d/<int:donasi_id>/', views.admin_detail, name='admin-detail'),
]
