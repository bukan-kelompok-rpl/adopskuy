from base64 import b64encode

from django.db import models
from django.utils.safestring import mark_safe

from akun.models import User


class Donasi(models.Model):

    class StatusDonasi(models.TextChoices):
        DISETUJUI = 'setuju', 'Disetujui'
        DITOLAK = 'tolak', 'Ditolak'

    judul = models.CharField(max_length=256)
    gambar = models.BinaryField(null=True, blank=True)
    deskripsi = models.TextField()
    target = models.BigIntegerField()
    dana_terkumpul = models.BigIntegerField(blank=True, default=0)
    banyak_pendonasi = models.IntegerField(blank=True, default=0)
    pemilik = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        null=True,
        blank=True)
    admin_penyetuju = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='menyutujui_donasi',
        null=True,
        blank=True)
    status_donasi = models.CharField(
        max_length=16,
        choices=StatusDonasi.choices,
        null=True,
        blank=True)

    def __str__(self):
        return self.judul

    def get_image_data(self):
        if self.gambar == None:
            return ""
        return mark_safe('data:image/png;base64,{}'.format(
            b64encode(self.gambar).decode('utf8')))
