function btnDonate() {
  $(".grey-screen").show();
  $(".card-centered").show();
  $(".choose-nominal").show();
  $(".choose-bayar").hide();
}

function closePopup() {
  $(".grey-screen").hide();
  $(".card-centered").hide();
}

$(document).ready(function () {
  $("input[type=radio][name=nominal]").click(function () {
    $(".choose-nominal").hide();
    $(".choose-bayar").show();
  });
  $("input[type=radio][name=bayar]").click(function () {
    $("#form-popup").submit();
  });
});
