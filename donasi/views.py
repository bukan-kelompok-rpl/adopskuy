from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required

from .models import Donasi


def home(request):
    context = {
        'page_title': 'Donasi',
        'all_donasi': Donasi.objects.filter(status_donasi=Donasi.StatusDonasi.DISETUJUI),
    }

    return render(request, 'donasi/list.html', context)


def detail(request, donasi_id):
    donasi = get_object_or_404(Donasi, id=donasi_id)

    context = {
        'page_title': f'Donasi {donasi.judul}',
        'donasi': donasi,
    }

    return render(request, 'donasi/detail.html', context)


def bayar(request):
    donasi_id = request.POST['donasi_id']
    donasi = get_object_or_404(Donasi, id=donasi_id)

    context = {
        'page_title': f'Kirim Donasi ke {donasi.judul}',
        'nominal': request.POST['nominal'],
        'bank': request.POST['bayar'],
        'donasi': donasi,
    }

    return render(request, 'donasi/bayar.html', context)

@login_required(login_url="akun:login")
def admin_home(request):
    if not request.user.admin:
        raise PermissionDenied

    context = {
        'page_title': 'Admin Donasi',
        'unverified_donasi': Donasi.objects.filter(status_donasi=None),
        'donasi_diterima': Donasi.objects.filter(status_donasi=Donasi.StatusDonasi.DISETUJUI),
        'donasi_ditolak': Donasi.objects.filter(status_donasi=Donasi.StatusDonasi.DITOLAK),
    }

    return render(request, 'donasi/admin_list.html', context)

@login_required(login_url="akun:login")
def admin_detail(request, donasi_id):
    if not request.user.admin:
        raise PermissionDenied

    donasi = get_object_or_404(Donasi, id=donasi_id)

    if request.method == 'POST':
        status = request.POST.get('status')
        if status == 'TERIMA':
            donasi.status_donasi = Donasi.StatusDonasi.DISETUJUI
            donasi.admin_penyetuju = request.user
            donasi.save()
        elif status == 'TOLAK':
            donasi.status_donasi = Donasi.StatusDonasi.DITOLAK
            donasi.admin_penyetuju = request.user
            donasi.save()
        return redirect('donasi:admin-home')


    context = {
        'page_title': f'Setujui {donasi.judul}',
        'donasi': donasi,
    }

    return render(request, 'donasi/admin_detail.html', context)

@login_required(login_url="akun:login")
def ubah_gambar(request):
    donasi_id = request.POST.get('donasi_id')
    donasi = get_object_or_404(Donasi, id=donasi_id)

    if request.user != donasi.pemilik:
        raise PermissionDenied

    donasi.gambar = request.FILES['gambar'].file.read()
    donasi.save()

    return redirect('donasi:detail', donasi_id)

def kirim(request, donasi_id):
    nominal = request.POST.get('nominal')

    try:
        donasi = Donasi.objects.get(id=donasi_id)
    except ObjectDoesNotExist:
        return redirect("donasi:home")

    donasi.dana_terkumpul += int(nominal)
    donasi.save()

    return redirect('donasi:detail', donasi_id)
