from akun.models import User
from django.db import models
from django.dispatch import receiver
import os

# Create your models here.

def get_image_path(instance, filename):
    return os.path.join('artikel', str(instance.penulis.id), filename)

class Artikel(models.Model):
    id_artikel = models.AutoField(primary_key=True)
    judul = models.CharField(max_length=256)
    isi = models.TextField()
    penulis = models.ForeignKey(User, 
        on_delete=models.CASCADE, 
        null=True, 
        blank=True
    )
    gambar = models.ImageField(
        upload_to=get_image_path,
        default = "static/artikel/artikel-jumbo.png"
    )
    tanggal_publikasi = models.DateTimeField(auto_now_add=True)
    terakhir_dimodifikasi = models.DateTimeField(auto_now=True)

    def get_id(self):
        return self.id_artikel
        

@receiver(models.signals.post_delete, sender=Artikel)
def auto_delete_gambar_on_delete(sender, instance, **kwargs):
    """
    Deletes gambar from filesystem
    when corresponding `Artikel` object is deleted.
    """
    if instance.gambar:
        if os.path.isfile(instance.gambar.path):
            os.remove(instance.gambar.path)

