from django.core import paginator
from django.core.paginator import Paginator
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from artikel.forms import ArtikelForm
from artikel.models import Artikel

# Create your views here.
def home(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            form = ArtikelForm(request.POST or None, request.FILES or None)
            if form.is_valid():
                artikel = form.save(commit=False)
                artikel.penulis = request.user
                artikel.save()
                return JsonResponse({"success": True})
    else:
        form = ArtikelForm()
    
    artikel_list = get_artikel_order_by('-terakhir_dimodifikasi')
    paginator = Paginator(artikel_list, 5)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'page_title':'Artikel',
        'page_obj': page_obj,
        'artikel_form':form
    }

    return render(request, 'artikel/home.html', context)


def artikel(request, id_artikel):
    artikel_obj = get_artikel(id_artikel)
    random_artikel = get_artikel_order_by("?")[:5]

    if request.method == 'POST':
        if request.user.is_authenticated:
            form = ArtikelForm(request.POST or None, request.FILES or None, instance=artikel_obj)
            if form.is_valid():
                form.save()
                return JsonResponse({"success": True})
    else:
        form = ArtikelForm(instance=artikel_obj)

    context = {
        'page_title':'Mengerti Hewan Peliharaanmu',
        'artikel_obj':artikel_obj,
        'artikel_form':form,
        'random_artikel':random_artikel
    }

    return render(request, 'artikel/artikel.html', context)


def hapus_artikel(request, id_artikel):
    artikel_obj = get_artikel(id_artikel)
    artikel_obj.delete()
    return HttpResponseRedirect("/artikel/")


def get_all_artikel():
    return Artikel.objects.all()


def get_artikel_order_by(order_by):
    return Artikel.objects.order_by(order_by)


def get_artikel(id_artikel):
    return get_object_or_404(Artikel, id_artikel=id_artikel)