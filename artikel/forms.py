from django import forms
from artikel.models import Artikel

class ArtikelForm(forms.ModelForm):
    class Meta:
        model = Artikel

        fields = [
            'judul', 
            'isi',
            'gambar'
        ]

        labels = {
            'judul':'Judul',
            'isi':'Isi Artikel',
            'gambar':'Gambar'
        }

        widgets = {
            'judul':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Masukkan judul artikel'
                }
            ),
            'isi':forms.Textarea(
                attrs = {
                    'class':'form-control'
                }
            ),
            'gambar':forms.ClearableFileInput(
                attrs = {
                    'onchange':'sub(this)'
                }
            )
        }