from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from artikel.views import home, artikel, hapus_artikel

app_name = 'artikel'

urlpatterns = [
    path('', home, name='home'),
    path('view/<int:id_artikel>/', artikel, name='artikel'),
    path('delete/<int:id_artikel>/', hapus_artikel, name='hapus_artikel')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)