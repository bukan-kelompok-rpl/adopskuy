from django.urls import path, include, re_path
from adopsi.views import *
from django.conf.urls.static import static
from django.conf import settings


app_name = 'adopsi'

urlpatterns = [
    path(route="",view=main_view, name="adopsi"),
    path('view/<int:id_adopsi>/', adopsi, name='view'),
    path('delete/<int:id_adopsi>/', hapus_adopsi, name='hapus_adopsi')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

