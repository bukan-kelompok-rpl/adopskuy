from django.core import paginator
from django.core.paginator import Paginator
from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from adopsi.forms import AdopsiForm
from adopsi.models import Adopsi


# Create your views here.
def main_view(request):
    if request.method == 'POST':
        if request.user.is_authenticated:
            form = AdopsiForm(request.POST or None, request.FILES or None )
            if form.is_valid():
                adopsi = form.save(commit=False)
                adopsi.pemilik = request.user
                adopsi.save()
                return JsonResponse({"success":True})
    else:
        form = AdopsiForm()
    
    adopsi_list = get_all_adopsi()
    paginator = Paginator(adopsi_list, 4)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'page_title': 'Adopsi',
        'page_obj': page_obj,
        'adopsi_form': form,
        'adopsi_list' : adopsi_list
    }

    return render(request, 'main.html', context)


def adopsi(request, id_adopsi):
    adopsi_obj = get_adopsi(id_adopsi)

    if request.method == "POST":
        if request.user.is_authenticated:
            form = AdopsiForm(request.POST or None, request.FILES or None, instance=adopsi_obj)
            if form.is_valid():
                form.save()
                return JsonResponse({"success":True})
    else:
        form = AdopsiForm(instance=adopsi_obj)
    
    context = {
        "page_title": "Adopsi",
        "adopsi_obj": adopsi_obj,
        "adopsi_form": form
    }

    return render(request, 'full_desc.html', context)

def hapus_adopsi(request, id_adopsi):
    adopsi_obj = get_adopsi(id_adopsi)
    adopsi_obj.delete()
    return HttpResponseRedirect("/adopsi/")

def get_all_adopsi():
    return Adopsi.objects.all()

def get_adopsi(id_adopsi):
    return get_object_or_404(Adopsi, id_adopsi=id_adopsi)

def get_adopsi_filter(name_filter):
    return Adopsi.objects.filter(judul__icontains=name_filter)