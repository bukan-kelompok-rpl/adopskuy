from django.db import models
from akun.models import User
from django.dispatch import receiver
import os

# Create your models here.

def get_image_path(instance, filename):
    return os.path.join('adopsi', str(instance.pemilik.id), filename)

class Adopsi(models.Model):
    id_adopsi = models.AutoField(primary_key=True)
    judul = models.CharField(max_length=100)
    deskripsi = models.TextField()
    pemilik = models.ForeignKey(User,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    gambar = models.ImageField(
        upload_to=get_image_path,
        default = "static/adopsi/placeholder.png"
    )

    def get_id(self):
        return self.id_artikel

@receiver(models.signals.post_delete, sender=Adopsi)
def auto_delete_gambar_on_delete(sender, instance, **kwargs):
    """
    Deletes gambar from filesystem
    when corresponding `Adopsi` object is deleted.
    """
    if instance.gambar:
        if os.path.isfile(instance.gambar.path):
            os.remove(instance.gambar.path)
