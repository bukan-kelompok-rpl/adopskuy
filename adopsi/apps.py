from django.apps import AppConfig


class AdopsiConfig(AppConfig):
    name = 'adopsi'
