from django import forms
from adopsi.models import Adopsi

class AdopsiForm(forms.ModelForm):
    class Meta:
        model = Adopsi

        fields = [
            'judul',
            'deskripsi',
            'gambar'
        ]

        labels = {
            'judul':'Judul',
            'deskripsi':'Deskripsi Hewan',
            'gambar':'Gambar'
        }

        widgets = {
            'judul':forms.TextInput(
                attrs = {
                    'class':'form-control',
                    'placeholder':'Masukkan judul post Adopsi'
                }
            ),
            'deskripsi':forms.Textarea(
                attrs = {
                    'class':'form-control'
                }
            ),
            'gambar':forms.ClearableFileInput(
                attrs = {
                    'onchange':'sub(this)'
                }
            )
        }